<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_users_change_field_username_type extends Migration
{
	/**
	 * @var string The name of the database table
	 */
	private $table_name = 'users';

	/**
	 * @var array The table's fields
	 */
	private $new_fields = array(
        'username' => array(
            'type'       => 'VARCHAR',
            'constraint' => 100,
            'null'       => false,
        ),
	);
        private $old_fields = array(
        'username' => array(
            'type'       => 'VARCHAR',
            'constraint' => 30,
            'null'       => false,
        ),
	);

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->modify_column($this->table_name, $this->new_fields);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
		$this->dbforge->modify_column($this->table_name, $this->old_fields);
	}
}